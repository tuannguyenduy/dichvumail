var url = '<?php echo base_url(); ?>';
$(document).ready(function(){
	$('a[href="#modal-login"]').on("click", function(){
		$('#form-login')[0].reset();
		$('.text-danger').css('display','none');
		$('#btnlogin').attr('disabled', false);
	});
	$('a[href="#modal-register"]').on("click",function(){
		$('#modal-login').modal('hide');
		$('#form-register')[0].reset();
		$('.text-danger').css('display','none');
	});
	$('a[href="#modal-forgotpass"]').on("click", function(){
		$('#modal-login').modal('hide');
		$('#form-forgotpass')[0].reset();
		$('.text-danger').css('display','none');
	});

});

function login(){
	$('#btnlogin').text('Đăng nhập');
	$('#btnlogin').attr('disabled', true);

	var formData = new FormData($('#form-login')[0]);
	$.ajax({
		url : "home/login",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		dataType : "json",
		success: function(data){
			if (data.login == false) {
				alert('Tên đăng nhập hoặc mật khẩu không chính xác!');
						$('#btnlogin').text('Đăng nhập');
						$('#btnlogin').attr('disabled', false);

					}
					if (data.status_nofi == true) {
						alert('đăng nhập thành công!');
						$('#modal-login').modal('hide'); // hide bootstrap modal


					}
					if (data.status_nofi == false) {
						$.each(data.messages , function(i, val){
							var element = $('[name = "'+ i +'"]');
							element.closest('div.form-group')
							.addClass(val.lenght > 0 ? 'has-error' : '')
							.find('.text-danger').remove();
							element.after(val);
						});
					}
					 $('#btnlogin').text('Đăng nhập'); //change button text
            		 $('#btnlogin').attr('disabled',false); //set button enable 

            		},
            		error: function (jqXHR, textStatus, errorThrown)
            		{

            			console.log(jqXHR);
            			console.log(textStatus);
            			console.log(errorThrown);
			            $('#btnlogin').text('Đăng nhập'); //change button text
			            $('#btnlogin').attr('disabled',true); //set button enable 

			        }

			    });
}
function register()
{
	$('#btnregister').text('Đăng ký');
	$('#btnregister').attr('disabled', true);

	var formData = new FormData($('#form-register')[0]);
	$.ajax({
		url : "home/register",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		dataType : "json",
		success : function(data){
			if (data.status_nofi == true) {
				alert('Đăng ký thành công!');
				$('#form-register')[0].reset();
				$('#modal-register').modal('hide'); // hide bootstrap modal

			}
			else{
				$.each(data.messages , function(i, val){
					var element = $('[name = "'+ i +'"]');
					element.closest('div.form-group')
					.addClass(val.lenght > 0 ? 'has-error' : '')
					.find('.text-danger').remove();
					element.after(val);
				});
				$('#btnregister').text('Đăng ký');
				$('#btnregister').attr('disabled', false);
			}
		},
		error: function (jqXHR, textStatus, errorThrown)
		{

			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$('#btnlogin').text('Đăng ký'); //change button text
			$('#btnlogin').attr('disabled',false); //set button enable 

		}

	});

}
function forgotpass()
{
	$('#btnforgotpass').text('xác nhận');
	$('#btnforgotpass').attr('disabled', true);
	var formData = new FormData($('#form-forgotpass')[0]);
	$.ajax({
		url : "home/forgot_password",
		type : "POST",
		data : formData,
		contentType: false,
		processData: false,
		dataType : "json",
		success : function(data){
			if (data.email_not_used == true) {
				alert('Email này chưa được sử dụng để đăng ký tài khoản, bạn vui lòng kiểm tra lại!');
				$('#btnforgotpass').text('xác nhận');
				$('#btnforgotpass').attr('disabled', false);
			}
			if (data.status_nofi == false) {
				$.each(data.messages , function(i, val){
					var element = $('[name = "'+ i +'"]');
					element.closest('div.form-group')
					.addClass(val.lenght > 0 ? 'has-error' : '')
					.find('.text-danger').remove();
					element.after(val);
				});
				$('#btnforgotpass').text('Xác nhận');
				$('#btnforgotpass').attr('disabled', false);
			}
			if (data.status_nofi == true) {
				alert('hệ thống đã gửi mã xác nhận vào email bạn vừa nhập, bạn vui lòng kiểm tra lại email');
				$('#modal-forgotpass').modal('hide');
				$('#form-forgotpass')[0].reset();

			}
		},
		error: function (jqXHR, textStatus, errorThrown)
		{

			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$('#btnforgotpass').text('Xác nhận'); //change button text
			$('#btnforgotpass').attr('disabled',false); //set button enable 

		}

	});
}