var save_method; //for save method string
var table;
var base_url = '<?php echo base_url();?>';

// function add_elenment(){
//     var old_pro= $('.add-select .add-item:first-child').html();
//     console.log(old_pro);
//     $(".add-select ").append('<div class="add-item">'+old_pro+'</div>');
//     $(".add-select .add-item:last-child").append('<button type="button" class="btn btn-default btn-remove"><i class="fa fa-trash"></i></button><button type="button" class="btn btn-danger btn_view" onclick=" add_elenment()"><i class="fa fa-plus" aria-hidden="true"></i></button>');
// }
// jQuery('body').on("click",".btn-remove",function(event) {

//     if (confirm('Bạn có muốn xóa dịch vụ này?')) {
//         jQuery(this).parents(".add-item").remove();
//     }
// });

$(document).ready(function(){


    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            { 
                "targets": [ -2 ], //2 last column (photo)
                "orderable": false, //set not orderable
            },
            ],

        });

    
});
// select2
$(document).ready(function() {
    $('#customer_id').select2();
});




function add_inv()
{
    save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.text-danger').css('display','none'); // clear error class
        $('[name="status"]').removeAttr('checked'); // clear checked
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Thêm hóa đơn mới'); // Set Title to Bootstrap modal title
    }
    function edit_inv(id)
    {
        save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.text-danger').css('display','none'); // clear error class

    //Ajax Load data from ajax
    $.ajax({
        url : "ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="customer_id"]').val(data.customer_id);
            $('[name="package_id"]').val(data.package_id);
            $('[name="quantity"]').val(data.quantity);
            $('[name="total"]').val(data.total);
            $('[name="payment_id"]').val(data.payment_id);
            $('[name="status"]').val(data.status);  
            if (data.status == 1) {
                $('[name="status"]').attr('checked','checked');
            }
            else{
                $('[name="status"]').removeAttr('checked');
            }
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Sửa thông tin hóa đơn'); // Set title to Bootstrap modal title



        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
        table.ajax.reload(null,false); //reload datatable ajax 
    }
    function save()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;

        if(save_method == 'add') {
            url = "ajax_add";
        } else {
            url = "ajax_update";
        }

    // ajax adding data to database

    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if (data.error == false) {
                alert('Tài khoản này không đủ tiền, vui lòng nạp thêm!');
                $('#modal_form').modal('hide');
            }
            if(data.status_nofi == true) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            if (data.status_nofi == false) {}{
                $.each(data.messages, function(i, val){

                    // $('[name="' + i + '"]').closest('.form-group').append(val);
                    var element = $('[name = "'+ i +'"]');
                    element.closest('div.form-group')
                    .addClass(val.lenght > 0 ? 'has-error' : '')
                    .find('.text-danger').remove();
                    element.after(val);
                    


                });
                
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
// js delete
function delete_inv(id)
{
    if(confirm('bạn có chắc muốn xóa bản ghi này không?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Xóa không thành công!');
            }
        });

    }
}


