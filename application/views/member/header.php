 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>DỊCH VỤ MAIL| DỊCH VỤ MAIL </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>public/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>public/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>public/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>public/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url(); ?>public/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url(); ?>public/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url(); ?>public/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- dataTable -->
    <link href=" <?php echo base_url();?>public/Datatable/css/jquery.dataTables.min.css" rel="styleshee" >
    <link href=" <?php echo base_url();?>public/Datatable/css/dataTables.bootstrap.min.css" rel="stylesheet" >

  </head>
  <body>
      <nav class="navbar navbar-default navbar-inverse" role="navigation">
          <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">Trang chủ</a>
              </div>
      
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse navbar-ex1-collapse">
                  <ul class="nav navbar-nav">
                      <li class="active"><a href="#">Dịch vụ</a></li>
                      <li><a href="#">Gói cước</a></li>
                  </ul>
                  <form class="navbar-form navbar-right" role="search">
                      <div class="form-group">
                          <input type="text" class="form-control" placeholder="Tìm kiếm">
                      </div>
                      <button type="submit" class="btn btn-default">Tìm</button>
                  </form>
                  <ul class="nav navbar-nav navbar-right">
                      <li><a href="#">About us</a></li>
                      <li class="dropdown">
                          <a href="fb.com" class="dropdown-toggle" data-toggle="dropdown">Tài khoản <b class="caret"></b></a>
                          <ul class="dropdown-menu">
        
                              <li><a href="<?php echo base_url(); ?>admin">Quản trị</a></li>
                              <li><a data-toggle="modal" href='#modal-login'>Người dùng</a></li>
                              <li><a href='<?php echo base_url(); ?>home/profile'>Thông tin cá nhân</a></li>
    
                          </ul>
                      </li>
                  </ul>
              </div><!-- /.navbar-collapse -->
          </div>
      </nav>
      <div class="modal fade" id="modal-login">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title text-center">ĐĂNG NHẬP</h4>
        </div>
        <div class="modal-body form">
          <?php echo validation_errors(); ?>
          <?php echo form_open('',['id'=>'form-login','class'=>'']) ?>
          <!-- <legend>Form title</legend> -->
          
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" name="email" placeholder="Tên đăng nhập">
          </div>
          <div class="form-group">
            <label for="password">Mật khẩu:</label>
            <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
          </div>
          <div class="form-group pull-right">
            <label for="">Bạn chưa có tài khoản? </label>
            <a href="#modal-register" data-toggle="modal" >Đăng kí,</a>
            <a href="#modal-forgotpass" data-toggle="modal" class="text-warning">Quên mật khẩu</a>
          </div>
          <br>
          
          <?php echo form_close() ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" id="btnlogin" onclick="login()" class="btn btn-primary">Đăng nhập</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal-register">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title text-center">Đăng ký người dùng</h4>
        </div>
        <div class="modal-body">
          <?php echo validation_errors(); ?>
          <?php echo form_open('',['id'=>'form-register','class'=>'']) ?>
            <input type="hidden" name="id">
            <div class="form-group">
              <label for="name">Họ và tên*</label>
              <input type="text" class="form-control" name="name" placeholder="Họ và tên">
            </div>
            <div class="form-group">
              <label for="email">Email*</label>
              <input type="text" class="form-control" name="email" placeholder="Email">
            </div>
            <div class="form-group">
              <label for="phone">Số điện thoại*</label>
              <input type="text" class="form-control" name="phone" placeholder="Số điện thoại">
            </div>
            <div class="form-group">
              <label for="password">Mật khẩu*</label>
              <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
            </div>
            <div class="form-group">
              <label for="address">Địa chỉ</label>
              <input type="text" class="form-control" name="address" placeholder="Địa chỉ">
            </div>
          
            
        
          <?php echo form_close(); ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" class="btn btn-primary" onclick="register()" id="btnregister">Đăng ký</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal-forgotpass">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title text-center">Quên mật khẩu?</h4>
        </div>
        <div class="modal-body">
          <?php echo validation_errors(); ?>
          <?php echo form_open('',['id'=>'form-forgotpass','class'=>'']) ?>
            <div class="form-group">
              <label>Vui lòng nhập vào địa chỉ email bạn đã đăng ký với chúng tôi vào ô bên dưới. cảm ơn!</label>
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <input type="text" class="form-control" name="email"  placeholder="Email">
            </div>
          
            
          
            
          <?php echo form_close(); ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" class="btn btn-primary" id="btnforgotpass" onclick="forgotpass()">Xác nhận</button>
        </div>
      </div>
    </div>
  </div>
  