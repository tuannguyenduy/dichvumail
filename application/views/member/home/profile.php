<?php require_once APPPATH.'views/member/header.php'; ?>
		<div class="container">
			<?php if ($this->session->userdata('user_info')) {  ?>
					 <div class="row">
					 	<div class="col-md-3">
					 		<h3><strong>THÔNG TIN CÁ NHÂN</strong></h3>
					 		<p>Số dư: <?php echo number_format($cus_info['amount']) ?> đ</p> 
					 		<a href=" <?php echo base_url(); ?>home/billing" class="btn btn-success">Lịch sử giao dịch</a> 
					 		<a href=" <?php echo base_url(); ?>home/logout" class="btn btn-warning">Đăng xuất</a>
					 	</div>
					 	<div class="col-md-9">
					 	 <?php
                            if($msg){
                                ?>
                                <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
                                    <?php echo $msg['content']; ?>
                                </div>
                                <?php
                            }

                            if (validation_errors()!='')
                            {
                                ?>
                                <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
                                <?php
                            }
                            ?>

					 	<form action="<?php echo base_url(); ?>home/profile" method="POST" role="form">
					 	<div class="form-group">
					 		<label for="name">Họ và tên:</label>
					 		<input type="text" class="form-control" name="name" value="<?php echo $cus_info['name']; ?>">
					 	</div>
					 	<div class="form-group">
					 		<label for="phone">Số điện thoại:</label>
					 		<input type="text" class="form-control" name="phone" value=" <?php echo $cus_info['phone'];  ?>">
					 	</div>
					 	<div class="form-group">
					 		<label for="address">Địa chỉ:</label>
					 		<input type="text" class="form-control" name="address" value=" <?php echo $cus_info['address']; ?>">
					 	</div>
					 	<div class="form-group">
					 		<label for="password">Mật khẩu:</label>
					 		<input type="password" class="form-control" name="password" 
					 		placeholder="để trống (nếu không đổi)" >
					 	</div>
					 	<div class="form-group">
					 		<label for="password_confirm">Xác nhận mật khẩu:</label>
					 		<input type="password" class="form-control" name="password_confirm" placeholder="Xác nhận mật khẩu">
					 	</div>

					 
					 	

					 	<button type="submit" class="btn btn-primary">Thay đổi</button>

					 </form>
					 	</div>
					 </div>
					
			<?php } ?>
			<?php if (empty($this->session->userdata('user_info'))) { ?>
				<strong>Bạn chưa đăng nhập để thực hiện chức năng này, vui lòng đăng nhập <a href=" <?php echo base_url(); ?>home " >tại đây</a></strong>

			<?php } ?>
			
			
			
			

		</div>
<?php require_once APPPATH.'views/member/footer.php'; ?>