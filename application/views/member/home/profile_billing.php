<?php require_once APPPATH.'views/member/header.php'; ?>
		<div class="container">
			<?php if ($this->session->userdata('user_info')) {  ?>
					 <div class="row">
					 	<div class="col-md-3">
					 		<h3><strong>THÔNG TIN CÁ NHÂN</strong></h3>
					 		<p>Số dư: <?php echo number_format($cus_info['amount']) ?> đ</p> 
                            <a href="<?php echo base_url(); ?>home/profile"  >Trang cá nhân</a> <br>
					 		<a href="<?php echo base_url(); ?>home/billing"  >Lịch sử giao dịch</a>  <br>
					 		<a href=" <?php echo base_url(); ?>home/logout" >Đăng xuất</a>
					 	</div>
					 	<div class="col-md-9">

					 	<table class="table table-bordered data_table" id="data_table">
                                <thead>
                                    <tr>
                                        <th width="5%">STT</th>
                                        <th width="20%">Thời gian</th>
                                        <th width="20%">Số tiền giao dịch</th>
                                        <th width="">Nội dung</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($billing):
                                        $count = 0;
                                        foreach ($billing as $item):
                                            $count++;
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $this->globals->get_dateformat($item['create_at'], 'd/m/Y H:i:s'); ?></td>
                                        <td><?php echo ($item['amount'])?number_format($item['amount']).' đ':FALSE; ?></td>
                                        <td>Nạp tiền vào tài khoản</td>
                                    </tr>
                                    <?php endforeach; endif; ?>
                                </tbody>
                            </table>
					 	</div>
					 </div>
					
			<?php } ?>
			<?php if (empty($this->session->userdata('user_info'))) { ?>
				<strong>Bạn chưa đăng nhập để thực hiện chức năng này, vui lòng đăng nhập <a href=" <?php echo base_url(); ?>home " >tại đây</a></strong>

			<?php } ?>
			
			
			
			

		</div>
<?php require_once APPPATH.'views/member/footer.php'; ?>