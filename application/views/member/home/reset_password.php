<?php require_once APPPATH.'/views/member/header.php'; ?>
<div class="container">
	<?php 
	if($check_exist == 1) :
		?>
		<?php
		if($msg){
			?>
			<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
				<?php echo $msg['content']; ?>
			</div>
			<?php
		}

		if (validation_errors()!='')
		{
			?>
			<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
			<?php
		}
		?>
		<form action="" method="POST" role="form">
			<legend>Dịch vụ mail thay đồi mật khẩu</legend>
		
			<div class="form-group">
				<label for="password">Mật khẩu mới:</label>
				<input type="password" class="form-control" name="password" placeholder="Mật khẩu">
			</div>
			<div class="form-group">
				<label for="confirm_password">Xác nhận mật khẩu mới:</label>
				<input type="password" class="form-control" name="confirm_password" placeholder="Xác nhận mật khẩu">
			</div>
		
		
			
		
			<button type="submit" class="btn btn-primary text-center">Xác nhận</button>
		</form>
		<?php else: ?>
             <br/>
            <div class="alert alert-info text-center">Liên kết không tồn tại hoặc đã hết hạn</div>
            <?php endif; ?>
	</div>
	<?php require_once APPPATH.'/views/member/footer.php'; ?>