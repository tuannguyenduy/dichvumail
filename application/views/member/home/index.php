<?php require_once APPPATH.'/views/member/header.php' ?>
<div class="container">
	<h3 class="text-center"><strong>DỊCH VỤ MAIL</strong></h3>
	<div class="row search_package">
		<form  method="GET" role="form">
			<h4 class="text-info"><strong>TÌM KIẾM GỌI DỊCH VỤ</strong></h4>
			<div class="col-md-4">
				<div class="form-group">
					<label for="quota_type">KIỂU GÓI TIN:</label>
					<select name="quota_type" class="form-control">
						<option value="0" <?php echo (isset($_GET['quota_type']) && $_GET['quota_type'] == '0') ? 'selected' : FALSE ?>>Không lặp lại</option>
						<option value="1" <?php echo (isset($_GET['quota_type']) && $_GET['quota_type'] =='1') ? 'selected' : FALSE ?> >Theo ngày</option>
						<option value="2" <?php echo (isset($_GET['quota_type']) && $_GET['quota_type'] =='2') ? 'selected' : FALSE ?>>Theo tháng</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="price">GIÁ:</label>
					<select name="price" class="form-control">
						<option value="0">Chọn mức giá</option>
						<option value="0-100"<?php echo (isset($_GET['price']) && $_GET['price'] == '0-100') ? 'selected' : FALSE; ?>>Dưới 100$</option>
						<option value="100-1000" <?php echo (isset($_GET['price']) && $_GET['price'] == '1000-1000') ? 'selected' : FALSE; ?> >Từ 100$ -> 1000$</option>
						<option value="1000-10000" <?php echo (isset($_GET['price']) && $_GET['price'] == '1000-10000') ? 'selected' : FALSE; ?>>Từ 1000$ -> 10000$</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
					<button type="submit" class="btn btn-danger" style="margin-top: 24px">Tìm kiếm</button>
			</div>



		</form>

	</div>
	<div class="row search-result">
	</div>
	<div class="row">
		<div class="col-md-6">
			<img src="https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=752&q=80">
		</div>
		<div class="col-md-6">
			<img src="https://images.unsplash.com/photo-1460925895917-afdab827c52f?ixlib=rb-1.2.1&auto=format&fit=crop&w=702&q=80">
		</div>
	</div>

	
</div>
<?php require_once APPPATH.'views/member/footer.php' ?>