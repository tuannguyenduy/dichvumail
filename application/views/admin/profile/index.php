<?php require_once APPPATH.'/views/admin/header.php' ?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="dashboard_graph">

				<div class="row x_title">
					<div class="col-md-6">
						<h3 >TRANG CÁ NHÂN</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h4>
							<strong>THÔNG TIN CÁ NHÂN</strong>
						</h4>
						<img src="https://images.unsplash.com/photo-1550831858-3c2581fed470?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80" style="width: 200px">

						<p>
							Họ và tên : <strong><?php echo $info_cur_user['fullname']; ?></strong>
						</p>
						<p>
							Email : <strong><?php echo $info_cur_user['email']?></strong>
						</p>
						<p>
							Số điện thoại: <strong><?php echo $info_cur_user['phone'] ?></strong>
						</p>
					</div>
					<div class="col-md-8">
						<form action="<?php echo base_url() ?>admin/profile/" method="POST" role="form">
							<h3>SỬA THÔNG TIN CÁ NHÂN</h3>
							<?php
							if($msg){
								?>
								<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
									<?php echo $msg['content']; ?>
								</div>
								<?php
							}

							if (validation_errors()!='')
							{
								?>
								<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
								<?php
							}
							?>
							<div class="form-group">
								<input type="hidden" class="form-control" name="username" value="<?php echo $info_cur_user['username'];  ?>">
							</div>
							
							<div class="form-group">
								<label for="fullname">Họ và tên:</label>
								<input type="text" class="form-control" name="fullname" value="<?php echo $info_cur_user['fullname']; ?>">
							</div>
							<div class="form-group">
								<label for="phone">Số điện thoại:</label>
								<input type="text" class="form-control" name="phone" value="<?php echo $info_cur_user['phone']; ?> ">
							</div>
							
							
							<div class="form-group">
								<label for="password">Mật khẩu:</label>
								<input type="password" class="form-control" name="password" placeholder="Mật khẩu (bỏ trống nếu không đổi)...">
							</div>
							<div class="form-group">
								<label for="password_confirm">Xác nhận mật khẩu:</label>
								<input type="password" class="form-control" name="password_confirm" placeholder="Xác nhận mật khẩu">
							</div>
							
							

							
							<button type="submit" class="btn btn-primary">Thay đổi</button>
						</form>
					</div>
				</div>

			</div>


		</div>				
	</div>
	<div class="row">

	</div>
</div>
<!-- /page content -->

<?php require_once APPPATH.'views/admin/footer.php' ?>