	<?php require_once APPPATH.'/views/admin/header.php' ?>
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="dashboard_graph">

					<div class="row x_title">
						<div class="col-md-6">
							<h3><?php echo $page_title; ?></h3>
						</div>
					</div>
					<div class="add-new text-right">
						<a href="<?php echo base_url() ?>admin/admin/export_user" class="btn btn-success">Xuất Excel</a>
						<button  onclick="reload_table()" class="btn btn-default"><i class="fa fa-exchange"></i></i> Tải lại</button>
					</div>
				</div>
				<?php
				if($msg){
					?>
					<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
						<?php echo $msg['content']; ?>
					</div>
					<?php
				}

				if (validation_errors()!='')
				{
					?>
					<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
					<?php
				}
				?>
				<div class="dashboard_graph">
					<form action=" <?php echo base_url(); ?>admin/invoice_detail/info/<?php echo $info['invoice_id'] ?>" method="POST" role="form">
						
						<div class="form-group">
							<label for="customer_id">Tên người dùng</label>
							<select name="customer_id" class="form-control" id="select2" >
								<?php foreach ($all_cus as $key ): ?>
									<option value="<?php echo $key['id'] ?>" <?php echo ($key['id'] == $info['customer_id']) ? "selected" : false ?>><?php echo $key['name'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="">Kiểu thanh toán</label>
							<select name="payment_id" class="form-control">
								<?php foreach ($all_pay as $key ): ?>
									<option value=" <?php echo $key['id'] ?> " <?php echo ($key['id'] == $info_inv['payment_id']) ? "selected" : false ?>><?php echo $key['name'] ?> </option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="status">Trạng thái</label> <br>
							<label class="switch">
								<input type="checkbox" class="form-control" name="status" <?php echo ($info_inv['status'] ==1 ) ? "checked" : false  ?>> 
								<span class="slider round"></span>

							</div>
							<!-- chi tiết hóa đơn theo id khách hàng -->
							<?php foreach ($info_inv_by as $info_n) { ?>
								<div class="form-group">
									<label for="">Tên gói dịch vụ</label>
									<select name="package_id[]" class="form-control select2" id="select2">
										<?php foreach ($all_pack_active as $key ) { ?>
											<option value="<?php echo $key['id'] ?>" <?php echo ($key['id'] == $info_n['package_id']) ? "selected" : '' ?>><?php echo $key['name'] ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label for="">Số lượng</label>
									<input type="number" min="0" class="form-control" name="quantity[]" value="<?php echo $info_n['quantity'] ?>" >
								</div>
							<?php } ?>



							<a href=" <?php echo base_url(); ?>admin/invoices/" class="btn btn-primary">Quay lại</a>
							<button type="submit" class="btn btn-danger" <?php echo ($info_inv['status'] ==1) ? "disabled" : false  ?>>Xác nhận</button>
						</form>

					</div>

				</div>				
			</div>

		</div>
		<script src="<?php echo base_url(); ?>public/admin/js/pages/invoice_detail.js"></script>



		<!-- /page content -->

		<?php require_once APPPATH.'views/admin/footer.php' ?>
