	<?php require_once APPPATH.'/views/admin/header.php' ?>
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="dashboard_graph">

					<div class="row x_title">
						<div class="col-md-6">
							<h3><?php echo $page_title; ?></h3>
						</div>
					</div>
					<div class="add-new text-right">
						<a href="<?php echo base_url() ?>admin/admin/export_user" class="btn btn-success">Xuất Excel</a>
						<button  onclick="add_inv()"  class="btn btn-primary"><i class="fa fa-plus"></i> Thêm </button>
						<button  onclick="reload_table()" class="btn btn-default"><i class="fa fa-exchange"></i></i> Tải lại</button>
					</div>
				</div>
				<div class="dashboard_graph">
					<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>ID</th>
								<th>Tên khách hàng</th>
								<th>Tổng tiền</th>
								<th>Kiểu thanh toán</th>
								<th>Trạng thái</th>
								<th>Ngày tạo</th>
								<th>Hành động</th>
							</tr>
						</thead>
						<tbody>
						</tbody>

						<tfoot>
							<tr>
								<th>ID</th>
								<th>Tên khách hàng</th>
								<th>Tổng tiền</th>
								<th>Kiểu thanh toán</th>
								<th>Trạng thái</th>
								<th>Ngày tạo</th>
								<th>Hành động</th>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>				
		</div>
		<div class="row">
			<div class="modal fade " id="modal_form">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal-title">Form thêm , sửa người dùng</h4>
						</div>
						<div class="modal-body form">
							<?php echo validation_errors(); ?>
							<?php echo form_open('',['id'=>'form','class'=>'']) ?>
							<input type="hidden" name="id">
							<div class="alert alert-danger" style="display:none">

							</div>
							<div class="form-group">
								<label for="customer_id">Tên khách hàng*</label> <br>
								<select class="form-control select2" style="width: 100%" name="customer_id">
									<?php foreach ($all_cus as $key ) { ?>

										<option value="<?php echo $key['id']; ?>"> <?php echo $key['name']; ?> </option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="payment_id">Kiểu thanh toán*</label>
								<select class="form-control" name="payment_id">
									<?php foreach ($all_pay as $key ) { ?>

										<option value="<?php echo $key['id'] ?>"><?php echo $key['name'] ?></option>
									<?php } ?>

								</select>
							</div>
							<div class="form-group">
								<label for="status">Trạng thái</label> <br>
								<label class="switch">
									<input type="checkbox" class="form-control" name="status" value="1"> 
									<span class="slider round"></span>

								</div>
								<button type="button" class="btn btn-danger btn_view"><i class="fa fa-plus" aria-hidden="true"></i></button>
								<div class="add-select">
									<div class="add-item">
										<div class="form-group select-old">
											<label for="">Tên gói dịch vụ</label> <br>
											<select name="package_id[]"  class="form-control select22 select2" style="width: 100%">
												<?php foreach ($all_pack_active as $key ) { ?>
													<option value=" <?php echo $key['id'] ?> "> <?php echo $key['name'] ?> </option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group">
											<label for="">Số lượng</label>
											<input type="number" min="0" class="form-control" name="quantity[]"  value="1">
										</div>

									</div>				
								</div>

								<?php echo form_close() ?>
							</div>
							<div class="modal-footer">

								<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
								
								<button type="button" id="btnsave" onclick="save()" class="btn btn-primary">Lưu</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>




		<!-- /page content -->

		<?php require_once APPPATH.'views/admin/footer.php' ?>
		<script src="<?php echo base_url(); ?>public/admin/js/pages/invoices.js"></script> -->
