<?php 
	/**
	 * 
	 */
	class Profile extends CI_Controller
	{
		protected $_data;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->model('Musers');
			$this->load->library('Get_data');
			$this->load->library('session');
			$this->load->library('form_validation');
		}
		public function index()
		{
			$id = $this->get_data->get_current_user();
			$msg = '';
			$this->form_validation->set_rules("fullname", "họ và tên", "required|trim");
			$this->form_validation->set_rules("phone", "số điện thoại", "required|trim|regex_match[/^[0-9]{10}$/]");
			if ($this->input->post('password')) {
				$this->form_validation->set_rules("password", "mật khẩu", "required|trim");
				$this->form_validation->set_rules("password_confirm", "xác nhận mật khẩu", "required|trim|matches[password]" );
			}
			if ($this->form_validation->run() == TRUE) {
				$data_update = array(
					'fullname' 	=> $this->input->post('fullname'),
					'phone'		=> $this->input->post('phone'),
				);
				if ($this->input->post('password')) {
					$data_update['password'] =  md5($this->input->post('password'));
				}
				$username = $this->input->post('username');
				$id 	  = $this->get_data->get_current_user();
				$update_stt = $this->Musers->update(array('id' => $id), $data_update);

				if ($update_stt) {
					$msg = array(
						"type" 		=> "success",
						"content"	=> "Cập nhật thành công"
					);
				}
				else{
					$msg = array(
						"type"  	=> "danger",
						"content"	=> "cập nhật không thành công thành công"
					);
				}
				$this->session->set_flashdata('update_msg',$msg);
				redirect('admin/profile');
			}
			$this->_data['msg'] 			= $this->session->flashdata('update_msg');
			$this->_data['info_cur_user']	= $this->Musers->get_users_by_id($id);
			$this->load->view('admin/profile/index.php', $this->_data);
		}

	}
	?>