<?php 
	/**
	 * 
	 */
	class Users extends CI_Controller
	{
		protected $_data;
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('Musers');
			$this->load->helper('url');
            $this->load->helper('security');
			$this->load->library('form_validation');
			$this->load->library('session');

		}
		public function index()
		{
			$this->_data['page_title'] 	= "Danh sách thành viên";
			$this->_data['head_title'] 	= "Quản lý thành viên";
			$this->load->view('admin/users/index.php', $this->_data);
		}
		public function ajax_list()
		{

			$list = $this->Musers->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $users) {
				$no++;
				$row = array();
				$row[] = $users->id;
				$row[] = $users->username;
				$row[] = $users->fullname;
				$row[] = $users->email;
				$row[] = $users->phone;
				$row[] = $users->create_at;
				$row[] = $users->update_at;

            //add html for action
				$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$users->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Sửa</a>
				<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_user('."'".$users->id."'".')"><i class="glyphicon glyphicon-trash"></i> Xóa</a>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->Musers->count_all(),
				"recordsFiltered" => $this->Musers->count_filtered(),
				"data" => $data,
			);
        //output to json format
			echo json_encode($output);
		}

		public function ajax_edit($id)
		{
			    $data = $this->Musers->get_by_id($id);
        	$data->create_at = ($data->create_at == '0000-00-00') ? '' : $data->create_at; // if 0000-00-00 set tu empty for datepicker compatibility
        	echo json_encode($data);
        }

        public function ajax_add()
        {
            $data = array('status_nofi' => FALSE, 'messages' => array());

            $this->form_validation->set_rules('username','tên đăng nhập','required|trim|xss_clean|is_unique[users.username]');
            $this->form_validation->set_rules('password','mật khẩu','trim|min_length[6]|max_length[20]|required');
            $this->form_validation->set_rules('fullname','họ và tên','required|trim');
            $this->form_validation->set_rules('email','Email','trim|required|max_length[254]|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('phone','số điện thoại','required|regex_match[/^[0-9]{10}$/]');
            $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
            if ($this->form_validation->run() == TRUE) {
              $data = array(
                 'id'			           => $this->input->post('id'),
                 'username' 		     => $this->input->post('username'),
                 'password' 		     => md5($this->input->post('password')),
                 'fullname' 		     => $this->input->post('fullname'),
                 'email' 		         => $this->input->post('email'),
                 'phone' 		         => $this->input->post('phone'),
                 'role' 			       => $this->input->post('role'),
                 'create_at' 	       => date("y-m-d h:i:s")

             );



              $insert = $this->Musers->save($data);
              $data['status_nofi'] = TRUE;


          }
          else{
             $data['messages']    = array(
                'username'          => form_error('username'),
                'password'          => form_error('password'),
                'fullname'          => form_error('fullname'),
                'email'             => form_error('email'),
                'phone'             => form_error('phone')   

            );
         }
         echo json_encode($data);







     }

     public function ajax_update()
     {

        $data = array('status_nofi' => FALSE, 'messages' => array());

        $this->form_validation->set_rules('username','tên đăng nhập','required|trim');
        if ($this->input->post('password')) {
          $this->form_validation->set_rules('password','mật khẩu','trim|min_length[6]|max_length[20]|required');
        }
        $this->form_validation->set_rules('fullname','họ và tên','required|trim');
        $this->form_validation->set_rules('email','Email','trim|required|max_length[254]|valid_email');
        $this->form_validation->set_rules('phone','số điện thoại','required|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if ($this->form_validation->run() == TRUE) 
        {

        $data = array(
          'id' => $this->input->post('id'),
          'username' => $this->input->post('username'),
          'fullname' => $this->input->post('fullname'),
          'email' => $this->input->post('email'),
          'phone' => $this->input->post('phone'),
          'role' => $this->input->post('role'),
          'update_at' => date("Y-m-d h:i:s")
        );
        if ($this->input->post('password')) {
          $data['password'] = md5($this->input->post('password'));
        }


       $this->Musers->update(array('id' => $this->input->post('id')), $data);
       $data['status_nofi'] = TRUE;

        }
        else{
            $data['messages'] = array(
                'username'      => form_error('username'),
                'password'      => form_error('password'),
                'fullname'      => form_error('fullname'),
                'email'         => form_error('email'),
                'phone'         => form_error('phone')
            );
        }
        echo json_encode($data);

    }


   public function ajax_delete($id)
   {
        //delete file
       $person = $this->Musers->get_by_id($id);
       $this->Musers->delete_by_id($id);
       echo json_encode(array("status" => TRUE));
   }

   public function check_username()
   {
    $username = trim($this->input->post('username'));
    $check    = $this->Musers->check_username($username);
    if (!empty($check)) {
        $this->form_validation->set_message('check_username', 'Username đã tồn tại');
    }
    if (empty($check)) {
        $this->form_validation->set_message('check_username', 'Trường Username là bắt buộc');
    }
}
public function check_email()
{
    $email = trim($this->input->post('email'));
    $check = $this->Musers->check_email_user($email);
    if (!empty($check)) 
    {
        $this->form_validation->set_message('check_email', 'Email đã tồn tại');
    }
    if (empty($check)) 
    {
        $this->form_validation->set_message('check_email', 'Trường Email là bắt buộc');
    }
}



}
?>