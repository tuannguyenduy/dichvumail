<?php 
	/**
	 * 
	 */
	class Invoice_detail extends CI_Controller
	{
		protected $_data;
		
		function __construct()
		{
			parent::__construct();
      $this->load->model('Minvoice_detail');
      $this->load->model('Minvoices');
      $this->load->model('Mcustomers');
      $this->load->model('Mcustomers_pack');
      $this->load->model('Mpayments');
      $this->load->model('Mpackages');
      $this->load->helper('url');
      $this->load->helper('security');
      $this->load->library('form_validation');
      $this->load->library('session');
      $this->load->library('Get_data');

    }

    public function info($id)
    {
      $msg = '';
      $this->_data['all_cus']             = $this->Mcustomers->get_all_customers();
      $this->_data['all_pack_active']     = $this->Mpackages->get_packages_active();
      $this->_data['all_pay']             = $this->Mpayments->get_all_payments();
      $this->_data['info_inv']            = $this->Minvoices->get_invoices_by($id);
      $this->_data['info_inv_by']         = $this->Minvoice_detail->get_invoices_inv($id); 
      $this->_data['info']                = $this->Minvoice_detail->get_by_id($id);
      $this->_data['page_title']          = "Chi tiết hóa đơn";
      $this->_data['head_title']          = "Quản lý chi tiết hóa đơn";

      // $this->form_validation->set_rules("package_id", "tên gói dịch vụ", "required|trim");
        $this->form_validation->set_rules("customer_id", "người dùng", "required|trim" );
        if ($this->form_validation->run() == TRUE) {
          $package_id = $this->input->post('package_id');
          $quantity   = $this->input->post('quantity');
          $count_total = 0;
          $delete = $this->Minvoice_detail->delete_by_id($id);
          foreach ($package_id as $k => $key) {

           $data_update_detail['package_id'] = $key;
           $data_update_detail['quantity']   = $quantity[$k];
           // lấy giá gói dịch vụ 
           $cost_pack                  = $this->get_data->get_price_pack_by($data_update_detail['package_id']);

          // lấy tiền đã trừ trong bảng hóa đơn
           $total_inv_by_id             = $this->get_data->get_total_inv($id);
          // tt
           $total = ($quantity[$k]) * $cost_pack;
           $count_total                += $total;
           $data_update_detail['total']              = $total ;

          // lấy tiền người dùng
           $amount_cus          = $this->get_data->get_amount_cus_by($_POST['customer_id']);
         // update số tiền người dùng
           $update_amount_user =  $amount_cus + $total_inv_by_id  - $data_update_detail['total'];

           if ($amount_cus >= $data_update_detail['total']) {
            $data_invoice = array(
              'customer_id'   => $this->input->post('customer_id'),
              'payment_id'    => $this->input->post('payment_id'),
              'status'        => $this->input->post('status') ? 1 : 0
            );
            $update_inv =  $this->Minvoices->update(array('id' => $id), $data_invoice);
          // insert customer package
            $check_status = $this->Mcustomers_pack->check_inv( $_POST['customer_id'], $key);
            if (($data_invoice['status'] ==1) && ($check_status == 0)) {
             $data_insert_cus_pack = array(
              'customer_id' => $_POST['customer_id'],
              'package_id'  => $key 
            );
             $insert_cus_pack =  $this->Mcustomers_pack->save($data_insert_cus_pack);
           }
          // end
           $data_update_inv = array(
            'invoice_id'    => $id,
            'customer_id'   => $_POST['customer_id'],
            'package_id'    => $key,
            'quantity'      => $quantity[$k],
            'total'         => $data_update_detail['total'], 
            'update_at'     => date("y-m-d h:i:s")

          );
           // update table invoices detail
           $update_inv_detail  = $this->Minvoice_detail->save($data_update_inv);
           if (isset($update_stt) || isset($update_inv_detail)) {
            $msg = array(
              "type"    => "success",
              "content" => "Cập nhật thành công"
            );
          }
          else{
            $msg = array(
              "type"    => "danger",
              "content" => "cập nhật không thành công"
            );
          }
          $this->session->set_flashdata('update_msg',$msg);


        }



      }
      $update_total_inv = $this->Minvoices->update(array('id' => $id), array('total' => $count_total));
      if ($data_invoice['status'] == 1) {
        $update_stt =  $this->Mcustomers->update(array('id' => $_POST['customer_id']), array('amount' => $update_amount_user));
      }
      redirect('admin/invoices/');
    }

  $this->_data['msg'] = $this->session->flashdata('update_msg');

  $this->load->view('admin/invoice_detail/index.php', $this->_data);


}






}
?>