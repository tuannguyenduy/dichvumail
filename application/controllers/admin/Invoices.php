<?php 
	/**
	 * 
	 */
	class Invoices extends CI_Controller
	{
		protected $_data;
		
		function __construct()
		{
			parent::__construct();
      $this->load->model('Minvoices');
      $this->load->model('Minvoice_detail');
      $this->load->model('Mcustomers');
      $this->load->model('Mcustomers_pack');
      $this->load->model('Mpayments');
      $this->load->model('Mpackages');
      $this->load->helper('url');
      $this->load->helper('security');
      $this->load->library('form_validation');
      $this->load->library('session');
      $this->load->library('Get_data');

    }
    public function index()
    {
      $this->_data['all_cus']             = $this->Mcustomers->get_all_customers();
      $this->_data['all_pack_active']     = $this->Mpackages->get_packages_active();
      $this->_data['all_pay']             = $this->Mpayments->get_all_payments();
      $this->_data['page_title']        	= "Danh sách hóa đơn";
      $this->_data['head_title']        	= "Quản lý hóa đơn";
      $this->load->view('admin/invoices/index.php', $this->_data);
    }
    public function ajax_list()
    {

     $list = $this->Minvoices->get_datatables();
     $data = array();
     $no = $_POST['start'];
     foreach ($list as $invoices) {
      $no++;
      $row = array();
      $row[] = $invoices->id;
      $row[] = $this->get_data->get_name_cus_by($invoices->customer_id);
      $row[] = number_format($invoices->total). ' đ';
      $row[] = $this->get_data->get_name_pay_by($invoices->payment_id);
      switch ($invoices->status) {
        case '0':
        $row[] = '<p class="label label-warning">Chưa thanh toán</p>';
        break;
        case '1':
        $row[] = '<p class="label label-success">Đã thanh toán</p>';
        break;
        default:

        break;
      }
      $row[] = $invoices->create_at;
            //add html for action
      $row[] = '<a class="btn btn-success" href="'.base_url().'admin/invoice_detail/info/'.$invoices->id.'">Chi tiết </a>
      <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_inv('."'".$invoices->id."'".')"><i class="glyphicon glyphicon-trash"></i> Xóa</a>';

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->Minvoices->count_all(),
      "recordsFiltered" => $this->Minvoices->count_filtered(),
      "data" => $data,
    );
        //output to json format
    echo json_encode($output);
  }

  public function ajax_edit($id)
  {
    $data = $this->Minvoices->get_by_id($id);
        	$data->create_at = ($data->create_at == '0000-00-00') ? '' : $data->create_at; // if 0000-00-00 set tu empty for datepicker compatibility
        	echo json_encode($data);
        }

        public function ajax_add()
        {
          $data = array('status_nofi' => FALSE, 'messages' => array());
          $this->form_validation->set_rules('customer_id','tên khách hàng','required|trim');
          $this->form_validation->set_rules('payment_id','kiểu thanh toán','required|trim');
          $this->form_validation->set_rules('quantity' , 'số lượng' ,'required|trim|xss_clean|numeric');
          $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
          if ($this->form_validation->run() == TRUE) {
            $data_insert = array(
              'customer_id' 		        => $this->input->post('customer_id'),
              'payment_id' 		          => $this->input->post('payment_id'),
              'status' 			            => $this->input->post('status') ? 1 : 0,
              'create_at' 	            => date("y-m-d h:i:s")

            );
            $insert              = $this->Minvoices->save($data_insert);
            if ($insert) {
              $package  = $this->input->post('package_id');
              $quantity = $this->input->post('quantity');
              $count_total = 0;
              foreach ($package as $k => $key) {
                $data_detail = array(
                  'invoice_id'    => $insert,
                  'customer_id'   => $data_insert['customer_id'],
                  'create_at'     => $data_insert['create_at']
                );
                $data_detail['package_id'] = $key;
                $data_detail['quantity']   = $quantity[$k];
                // check status hoa don va goi dich vu trong bang customer_pack
                $check_status = $this->Mcustomers_pack->check_inv($data_insert['customer_id'], $key);
                if (($data_insert['status'] ==1) && ($check_status == 0)) {
                 $data_insert_cus_pack = array(
                        'customer_id' => $data_detail['customer_id'],
                        'package_id'  => $key  
                 );
                 $insert_cus_pack =  $this->Mcustomers_pack->save($data_insert_cus_pack);
                }
                // lay gia tien goi hang
                $cost_pack                  = $this->get_data->get_price_pack_by($data_detail['package_id']);
                $total                      = ($quantity[$k]) * $cost_pack  ;
                $count_total                += $total;
                $data_detail['total']        = $total;    
                // lay so tien khach hang
                $amount_cus          = $this->get_data->get_amount_cus_by($data_insert['customer_id']);
                if ($amount_cus > $data_detail['total']) { 
                  $insert_detail       = $this->Minvoice_detail->save($data_detail);
                  $amount_after_add    = $amount_cus - $data_detail['total'];
                  $data_update_cus     = array('amount' => $amount_after_add);
                  if ($data_insert['status'] == 1) {
                     $this->Mcustomers->update(array('id' => $data_insert['customer_id']),$data_update_cus);
                  }
                  $data['status_nofi'] = TRUE;

                }
                else{
                  $data['error'] = FALSE;
                }





              }
              $this->Minvoices->update(array('id' => $data_detail['invoice_id']), array('total' => $count_total));


            }
            else{
              $data['error'] = FALSE;
            }



          }
          else{
           $data['messages']    = array(
            'customer_id'           => form_error('customer_id'),
            'package_id'            => form_error('package_id'),
            'quantity'              => form_error('quantity')

          );
         }
         echo json_encode($data);







       }

     public function ajax_delete($id)
     {
        //delete file
       $person = $this->Minvoices->get_by_id($id);
       $this->Minvoices->delete_by_id($id);
       $this->Minvoice_detail->delete_by_id($id);
       echo json_encode(array("status" => TRUE));
     }





   }
   ?>