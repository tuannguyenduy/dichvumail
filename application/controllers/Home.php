<?php 
	/**
	 * 
	 */
	class Home extends CI_Controller
	{
		protected $_data;
		function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->model('Mcustomers');
			$this->load->model('Mcustomers_pay');
			$this->load->model('Mpackages');
			$this->load->library('Globals');
			$this->load->library('Get_data');
			$this->load->library('session');
			$this->load->library('form_validation');
		}
		//
		public function index()
		{
			if ($this->input->get('quota_type')) {
				$quota_type = $this->input->get('quota_type');
			}
			if ($this->input->get('price')) {
				$price 		= $this->input->get('price');
			}
			$this->_data['all_cus'] 	= $this->Mcustomers->get_all_customers();
			$this->load->view('member/home/index.php', $this->_data);
		}
		public function login()
		{
			$data = array('status_nofi' => FALSE, 'messages' => array());
			$this->form_validation->set_rules("email", "email", "required|trim|valid_email");
			$this->form_validation->set_rules("password", "mật khẩu", "required|trim");
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if ($this->form_validation->run() == TRUE) {
				$data = array(
					'email' 		=> $this->input->post('email'),
					'password'		=> md5($this->input->post('password'))
				);
				if ($this->Mcustomers->check_cus_login($data['email'],$data['password']) == 0) {
					$data['login'] = FALSE;
				}
				else{
					$cus_info = $this->Mcustomers->get_customers_by('email',$data['email']);
					$data['status_nofi'] = TRUE;
					$this->session->set_userdata('user_login', $data['email']);
					$this->session->set_userdata('user_info', $cus_info);
					$login = $this->session->get_userdata();
				}

			}
			else{
				$data['messages'] = array(
					'email' 	=> form_error('email'),
					'password'	=> form_error('password')
				);
			}
			echo json_encode($data);
		}
		public function logout()
		{
			$this->session->unset_userdata('user_info');
			redirect('home');
		}
		public function register()
		{
			$data = array('status_nofi' => FALSE, 'messages' => array());
			$this->form_validation->set_rules("name", "họ và tên", "required|trim");
			$this->form_validation->set_rules("email", "email", "required|trim|valid_email|is_unique[customers.email]");
			$this->form_validation->set_rules("phone","số điện thoại","required|trim|regex_match[/^[0-9]{10}$/]");
			$this->form_validation->set_rules("password","mật khẩu","required|trim");
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if ($this->form_validation->run() == TRUE) {
				$data = array(
					'name'  	=> $this->input->post('name'),
					'email'		=> $this->input->post('email'),
					'phone'		=> $this->input->post('phone'),
					'password'	=> md5($this->input->post('password')),
					'address'	=> $this->input->post('address')
				);
				$this->Mcustomers->save($data);
				$data['status_nofi'] = TRUE;
			}
			else{
				$data['messages'] = array(
					'name' 		=> form_error('name'),
					'email'		=> form_error('email'),
					'phone'		=> form_error('phone'),
					'password'	=> form_error('password')
				);
			}
			echo json_encode($data);

		}
		public function forgot_password(){
			$data = array('status_nofi' => FALSE, 'messages' => array());
			$this->form_validation->set_rules("email","email","required|trim|valid_email");
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if ($this->form_validation->run() == TRUE) {
				$email 			= $this->input->post('email');
				$check_email 	= $this->Mcustomers->check_email_cus($email);
				if ($check_email > 0) {
					$row 		= $this->Mcustomers->get_customers_by('email',$email);
					$token 		= md5(uniqid());
					$data_update		= array(
						'forgot_code' => $token
					);
					$update_stt = $this->Mcustomers->update(array('id' => $row['id']),$data_update);
					if ($update_stt) {
						$subject = 'Dịch vụ mail - Lấy lại mật khẩu';
						$content = '<p>Chào bạn:</p>';
						$content.= '<p>Để đặt lại mật khẩu bạn vui lòng click vào link sau:</p>';
						$content.= '<p>'.base_url('home/reset_password/'.$email.'/'.$token).'</p>';
						$content.= '<p>Cảm ơn!</p>';
						$this->globals->sendMail($email, $subject, $content);

						$data['status_nofi'] = TRUE;
					}	
				}
				else{
					$data['email_not_used'] = TRUE;
				}
			}
			else{
				$data['messages'] = array(
					'email' => form_error('email')
				);
			}
			echo json_encode($data);

		}
		public function reset_password($email, $token)
		{
			$data = array('status_nofi' => FALSE, 'messages' => array());
			$check_email = $this->Mcustomers->get_customers_by('email', $email);
			$check_token = $this->Mcustomers->get_customers_by('forgot_code' , $token);
			if ($check_email && $check_token) {
				$this->_data['check_exist'] = 1;
				$msg = '';
				$this->form_validation->set_rules("password", "mật khẩu", "required|trim");
				$this->form_validation->set_rules("confirm_password", "nhắc lại mật khẩu","required|trim|matches[password]");
				if ($this->form_validation->run() == TRUE) {
					$data_update = array(
						'password' 		=> md5($this->input->post('password')),
						'forgot_code' 	=> ''
					);
					$update_stt = $this->Mcustomers->update(array('id' => $check_email['id']), $data_update);
					if ($update_stt)
					{
						$msg = array(
							"type" => "success",
							"content" => "Cập nhật thành công"
						);
					}
					else
					{
						$msg = array(
							"type" => "danger",
							"content" => "Cập nhật không thành công!"
						);
					}
					$this->session->set_flashdata('update_msg', $msg);

				}
				$this->_data['msg'] = $this->session->flashdata('update_msg');
			}
			else{
				$this->_data['check_exist'] = 0;
			}
			$this->load->view('member/home/reset_password.php', $this->_data);
		}
		public function profile()
		{
			$id    = $this->get_data->get_current_cus(); 
			$msg = '';
			$data = array('status_nofi' => FALSE, 'messages' => array());
			$this->form_validation->set_rules("name", "họ và tên", "required|trim");
			$this->form_validation->set_rules("phone", "số điện thoại", "required|trim|regex_match[/^[0-9]{10}$/]");
			$this->form_validation->set_rules("address","địa chỉ","required|trim");
			if ($this->input->post('passsword')) {
				$this->form_validation->set_rules("password","mật khẩu","required|trim");
				$this->form_validation->set_rules("password_confirm","xác nhận mật khẩu","required|trim|matches[password]");
			}
			if ($this->form_validation->run() == TRUE) {
				$data_update = array(
					'name'  	=> $this->input->post('name'),
					'phone'		=> $this->input->post('phone'),
					'address'	=> $this->input->post('address')
				);
				if ($this->input->post('password')) {
					$data_update['password'] = md5($this->input->post('password'));
				}
		

				$update_stt = $this->Mcustomers->update(array('id' => $id) ,$data_update); 
				if ($update_stt) {
					$msg = array(
						"type" 		=> "success",
						"content"	=> "Cập nhật thành công"
					);
				}
				else{
					$msg = array(
						"type" 		=> "danger",
						"content"	=> "cập nhật không thành công"
					);
				}
				$this->session->set_flashdata('update_msg',$msg);
				redirect('home/profile');

			}
			$this->_data['msg'] 		= $this->session->flashdata('update_msg');
			$this->_data['cus_info']	= $this->Mcustomers->get_cus_by($id);
			$this->load->view('member/home/profile.php', $this->_data);
		}
		public function billing()
		{
			$id    = $this->get_data->get_current_cus(); 
			$msg = '';
			$this->_data['page_title'] = 'Lịch sử giao dịch';
			$id 			= $this->get_data->get_current_cus();
			$billing 		= $this->Mcustomers_pay->get_cus_payment_by('customer_id', $id);
			$this->_data['billing'] = $billing;
			$this->_data['cus_info']	= $this->Mcustomers->get_cus_by($id);
			$this->load->view('member/home/profile_billing.php', $this->_data);
		}

	}

	